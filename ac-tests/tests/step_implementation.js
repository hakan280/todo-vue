/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press, text, focus,inputField, textBox, toRightOf } = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
    await openBrowser({ headless: 'true', args:['--no-sandbox', '--disable-setuid-sandbox'] })
});

afterSuite(async () => {
    await closeBrowser();
});

step("Goto todo-vue home page", async () => {
    await goto('127.0.0.1:8080');
});

step("Add a task <taskName>", async (taskName) => {
	await focus(textBox({class: "new-todo"}));
    await write(taskName);
    await press('Enter');
});

step("Page contains <content>", async (content) => {
    assert.ok(await text(content).exists());
});